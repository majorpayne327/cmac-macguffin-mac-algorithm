import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

import edu.rit.util.Packing;
import edu.rit.util.Hex;

public class cmac implements Mac{
	private byte[] secretKey;
	private byte[] k1;
	private byte[] k2;
	private long Rb;
	
	private byte[] msgBuffer;
	private int msgCount;
	private byte[] tag;
	private byte[] prev;
	private int num_blocks;
	private int blockSize;
	private long messageLength;
	private int bufferLength;
	private BlockCipher macguffin;
	
	public cmac(BlockCipher cipher, long msgLength){
		macguffin = cipher;
		blockSize = cipher.blockSize();
		Rb = 0x1BL; 							
		k1 = new byte[8];
		k2 = new byte[8];
		prev = Hex.toByteArray("0000000000000000");	//Set the bye array to be xor'd (initially zero)
		messageLength = msgLength;
		
		if(msgLength == 0){
			num_blocks = 1;
		}else{
			double temp = (double)msgLength / (double)blockSize;
			num_blocks = (int) Math.ceil(temp);
		}

		bufferLength = blockSize * num_blocks;
		msgBuffer = new byte[bufferLength]; 
	
	}
	/**
	 * Returns this MAC's digest size in bytes.
	 *
	 * @return  Digest size.
	 */
	public int digestSize(){
		return tag.length;
	}

	public byte[] getDigest(){
		return tag;
	}
	/**
	 * Returns this MAC's key size in bytes.
	 *
	 * @return  Key size.
	 */
	public int keySize(){
		return secretKey.length;
	}
	
	public byte[] getKey(){
		return secretKey;
	}
	public byte[] getKey1(){
		return k1;
	}
	
	public byte[] getKey2(){
		return k2;
	}
	/**
	 * Set the key for this MAC. <TT>key</TT> must be an array of bytes whose
	 * length is equal to <TT>keySize()</TT>.
	 *
	 * @param  key  Key.
	 */
	public void setKey(byte[] key){
		macguffin.setKey(key);
		byte[] cText = Hex.toByteArray("0000000000000000");			//Set up initial key by creating a cipher text from all zero's
		macguffin.encrypt(cText);
		secretKey = cText;
		subKeySetUp();
	}
	
	/**
	* Set subkey1 and subkey2 for this MAC.
	*
	*/
	public void subKeySetUp(){
		if(secretKey[0] == 0){		
			ByteBuffer bb = ByteBuffer.wrap(secretKey);
			long l = bb.getLong();
			long tempShift = l << 1;
			Packing.unpackLongBigEndian(tempShift, k1, 0);
		}else{
			ByteBuffer bb = ByteBuffer.wrap(secretKey);
			long l = bb.getLong();
			long tempShift = (l << 1) ^ Rb;
			Packing.unpackLongBigEndian(tempShift, k1, 0);
		}
		
		int msb = (int)k1[0];
		msb = msb & 0x80;
		if(msb == 0){
			ByteBuffer bb = ByteBuffer.wrap(k1);
			long l = bb.getLong();
			long tempShift = l << 1;
			Packing.unpackLongBigEndian(tempShift, k2, 0);
		}else{
			ByteBuffer bb = ByteBuffer.wrap(k1);
			long l = bb.getLong();
			long tempShift = (l << 1) ^ Rb;
			Packing.unpackLongBigEndian(tempShift,  k2,  0);
		}
	}
	
	/**
	 * Append the given byte to the message being authenticated. Only the least
	 * significant 8 bits of <TT>b</TT> are used.
	 *
	 * @param  b  Message byte.
	 */
	public void hash(int b){
		
		byte temp = (byte)(b & 0xff);										// Convert int to byte[]
		msgBuffer[msgCount++] = temp;	 					// Add temp to msgBuffer (in a temp array)
  
	}
	
	public byte[] getMsgBuffer(){
		return msgBuffer;
	}
	/**
	 * Obtain the message digest. <TT>d</TT> must be an array of bytes whose
	 * length is equal to <TT>digestSize()</TT>. The message consists of the
	 * series of bytes provided to the <TT>hash()</TT> method. The digest of the
	 * message is stored in the <TT>d</TT> array.
	 *
	 * @param  digest  Message digest (output).
	 */
	public void digest(byte[] d){
		//System.out.print("Input " + Hex.toString(msgBuffer));
		boolean even = true;
		byte[] block = new byte[blockSize];			
		
		int index;
		int count = 0; 
		while((count+1) != num_blocks){								//while not the last block
			index = count * blockSize;
			block = Arrays.copyOfRange(msgBuffer, index, index+blockSize);
			
			for(int i = 0; i < blockSize; i++){
				block[i] = (byte) (block[i] ^ prev[i]);				//xor the new message with the previous tag (initially zero)
			}
			macguffin.encrypt(block);
			//System.out.print("BLOCK " + Hex.toString(block) + "\n");
			
			prev = block;											//set the digest to the new output
			//System.out.print("PREV " + Hex.toString(prev) + "\n");
			count += 1;
		}
		
		//Last block
		if(messageLength % blockSize == 0 ){								//If even block size
			index = (count+1) * blockSize;
			block = Arrays.copyOfRange(msgBuffer, index, index + blockSize);
			
			for(int j = 0; j < block.length; j++){						//If the last block was of even blocklength size simply xor k1 and the previous tag
				block[j] = (byte) (block[j] ^ prev[j]);
				block[j] = (byte) (block[j] ^ k1[j]);
				
			}
			macguffin.encrypt(block);									//Encrypt last block
			//System.out.print("LAST " + Hex.toString(block) + "\n");
			
		}
		else{															//Otherwise buffer the block with 10...0
			
			long leftover = (long)bufferLength - messageLength;
			index = (count+1) * blockSize;
			block = Arrays.copyOfRange(msgBuffer, index, index + blockSize);	//Copy remaining bytes
			
			leftover = bufferLength - messageLength;						
			index = blockSize - (int)leftover;
			//System.out.print("INDEX " + index + " BLOCK " + block.length);
			block[index] = (byte)0x80;													//Buffer rest of block
			index++;				
			while(index != blockSize){
				block[index] = 0;
				index ++;
			}
			//System.out.print("LAST CHECK " + Hex.toString(block));
			for(int x = 0; x < blockSize; x++){									// xor the previous digest, k2, and current block
				block[x] = (byte) (block[x] ^ k2[x]);
				//System.out.print("\nXOR WITH K " + Hex.toString(block[x]));
				block[x] = (byte) (block[x] ^ prev[x]);	
				//System.out.print("\nXOR WITH PREV " + Hex.toString(block[x]));
				
			}
			macguffin.encrypt(block);											//Encrypt last block
		}
		
		for(int i = 0; i < blockSize; i++){										//Finally copy result into digest
			d[i] = block[i];
		}
		tag = d;
		
	}
	
	public static String read(String filename) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		String text = null;
		StringBuilder build = new StringBuilder();
		try{
			System.out.printf("Reading in plaintext\n");
			while((text = reader.readLine()) != null){
				build.append(text);
				//System.out.printf(text);
			}
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}
		System.out.printf("\n");
		reader.close();
		String str = build.toString();
		return str;
	}
	
	public static void main(String args[]){
		String plaintext = null;
		long msgLen = 1000000;
		byte[] key = Hex.toByteArray("00000000000000000000000000000000");	//Secret key
		
		BlockCipher cipher = new macguffin();								//Instantiate blockcipher
		
		try {
			plaintext = read("Test.txt");									//Read in the plaintext 
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		msgLen = plaintext.length();
		Mac mac = new cmac(cipher, msgLen);					//Instantiate mac with cipher, cipher block size, and number of blocks to be handled
		mac.setKey(key);												//Set mac key (calls subkey setup)
	    
		char[] hashing = plaintext.toCharArray();
		int hash;
		for(int i = 0; i <msgLen; i++){
			hash = Character.getNumericValue(hashing[i]);
			System.out.print(hash);
			mac.hash(hash);
		}
		byte[] dig = new byte[8];
		mac.digest(dig);
		System.out.printf("%s \n", "TAG");
		System.out.printf("%s \n",Hex.toString(dig));

	/*
		byte[] tempk = mac.getKey();
		byte[] tempk1 = mac.getKey1();
		byte[] tempk2 = mac.getKey2();
		System.out.printf("%s \n", "SECRETKEY");
		System.out.printf("%s \n", Hex.toString(tempk));
		
		System.out.printf("%s \n", "KEY1");
		System.out.printf("%s \n", Hex.toString(tempk1));
		
		System.out.printf("%s \n", "KEY2");
		System.out.printf("%s \n", Hex.toString(tempk2));
		*/					
		
	}

}