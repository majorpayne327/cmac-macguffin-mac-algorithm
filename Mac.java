public interface Mac
	{

	/**
	 * Returns this MAC's digest size in bytes.
	 *
	 * @return  Digest size.
	 */
	public int digestSize();

	public byte[] getDigest();
	/**
	 * Returns this MAC's key size in bytes.
	 *
	 * @return  Key size.
	 */
	public int keySize();

	/**
	 * Set the key for this MAC. <TT>key</TT> must be an array of bytes whose
	 * length is equal to <TT>keySize()</TT>.
	 *
	 * @param  key  Key.
	 */
	public void setKey
		(byte[] key);

	public byte[] getKey();
	public byte[] getKey1();
	public byte[] getKey2();
	/**
	 * Append the given byte to the message being authenticated. Only the least
	 * significant 8 bits of <TT>b</TT> are used.
	 *
	 * @param  b  Message byte.
	 */
	public void hash
		(int b);

	public byte[] getMsgBuffer();
	/**
	 * Obtain the message digest. <TT>d</TT> must be an array of bytes whose
	 * length is equal to <TT>digestSize()</TT>. The message consists of the
	 * series of bytes provided to the <TT>hash()</TT> method. The digest of the
	 * message is stored in the <TT>d</TT> array.
	 *
	 * @param  digest  Message digest (output).
	 */
	public void digest
		(byte[] d);

	}
