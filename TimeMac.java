import edu.rit.util.Hex;
import java.util.Random;

public class TimeMac {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String plaintext = null;
		byte[] digest = new byte[8];
		long runTime = 10000;
		long startTime = System.nanoTime();
		for(long j = 0; j < runTime; j++){
			int msgLen = 1000;
			byte[] key = Hex.toByteArray("00000000000000000000000000000000");	
			BlockCipher cipher = new macguffin();								
			Mac mac = new cmac(cipher, msgLen);										
			mac.setKey(key);													
		
			//Random generate = new Random();
			//int randomInt = 0;
			for(int i = 0; i < msgLen; i++){
			//randomInt = generate.nextInt(9);
			mac.hash(0);
			}
			mac.digest(digest);
		}
		long endTime = System.nanoTime();
		long duration = endTime - startTime;
		System.out.printf("Duration: %d \n", duration);
		System.out.printf("\n " + Hex.toString(digest));
		
	}

}
