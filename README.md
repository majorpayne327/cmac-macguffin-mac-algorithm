# CMAC and MacGuffin README

This project uses the PJ2 library. 

PJ2 executable distribution:
http://www.cs.rit.edu/~ark/pj2.shtml#download
http://www.cs.rit.edu/~ark/pj2_20131202.jar

rename pj2_20131202.jar -> /lib/pj2.jar

Compile:
javac -cp "lib/pj2.jar;." macguffin.java

Run:
java -cp "lib/pj2.jar;." macguffin