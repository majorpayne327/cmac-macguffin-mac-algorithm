import edu.rit.util.Packing;
import edu.rit.util.Hex;

public class TimeBlockCipher{

	private static final long N = 1000000;
	
	public static void main(String args[]){	
		BlockCipher cipher = new macguffin();
		byte[] key = Hex.toByteArray("00000000000000000000000000000000");
		byte[] text = null;
		
		long startTime = System.nanoTime();
		
		cipher.setKey(key);
		
		for(long i=0; i<N; i++){
			text = Hex.toByteArray("0000000000000000");
			cipher.encrypt(text);	
		}
		
		long endTime = System.nanoTime();
		long duration = endTime - startTime;
		
		System.out.printf("N: %,16d iterations\n",N);		
		System.out.printf("duration: %,16d ns\n",duration);
		System.out.printf("result: %s \n",Hex.toString(text));		
	}
}